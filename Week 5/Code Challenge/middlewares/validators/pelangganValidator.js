const {
    check,
    validationResult,
    matchedData,
    sanitize,
  } = require("express-validator");
  const { pelanggan } = require('../../models')

  
  module.exports = {
    getOne: [
      check('id').custom(value => {
        return pelanggan.findOne({
          _id: value
        }).then(result => {
          if (!result) {
            throw new Error('ID Pelanggan tidak ada!')
          }
        })
      }),
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      }
    ],
    create: [
      check('nama').custom(value => {
        return pelanggan.findOne({
          nama: value
        }).then(result => {
          if (result) {
            throw new Error('Nama Pelanggan sudah ada!')
          }
        })
      }),
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      }
    ],
   
  
    update: [
      check("id").custom((value) => {
        return pelanggan.findOne({
            _id: value,
          })
          .then((result) => {
            if (!result) {
              throw new Error("ID Pelanggan tidak ada");
            }
          });
      }),
      check('nama').isLength({ min: 3 }),
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped(),
          });
        }
        next();
      },
    ],
  
    delete: [
      check('id').custom(value => {
        return pelanggan.findOne({
          _id: value
        }).then(result => {
          if (!result) {
            throw new Error('ID Pelanggan tidak ada!')
          }
        }).catch(error => {
          throw new Error('ID Pelanggan tidak ada!')
        })
      }),
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      }
    ]
  };
  