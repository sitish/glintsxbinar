const {check, validationResult, matchedData, sanitize} = require('express-validator');
const { pemasok } = require('../../models')

module.exports = {
  create: [
    check('nama').custom(value=> {
      return pemasok.findOne({
        nama: value
      }).then(result => {
        if(result){
          throw new Error('Nama pemasok sudah ada')
        }
      })
    }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  update: [
    check("id").custom((value) => {
      return pemasok.findOne({
          _id: value,
        })
        .then((result) => {
          if (!result) {
            throw new Error("ID Pemasok tidak ada");
          }
        });
    }),
    check('nama').isLength({ min: 3 }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped(),
        });
      }
      next();
    },
  ],


  delete: [
    check('id').custom(value => {
      return pemasok.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('ID Pemasok tidak ada!')
        }
      }).catch(error => {
        throw new Error('ID Pemasok tidak ada!')
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ]
}