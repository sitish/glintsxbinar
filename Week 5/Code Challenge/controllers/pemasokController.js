const {pemasok} = require('../models')

class PemasokController {
  async getAll(req,res) {
    pemasok.find({}).then(result => {
        res.json({
          status: "success",
          data: result
        })
      });
  }

  async getOne(req,res) {
    pemasok.findOne({
        _id: req.params.id
      }).then(result => {
        res.json({
          status: "success",
          data: result
        })
      })
  }
  async create(req,res) {
    pemasok.create({
      nama:req.body.nama
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async update(req, res) {
    pemasok.findOneAndUpdate({
      _id: req.params.id
    }, {
        "nama": req.body.nama
    }).then(() => {
      return pemasok.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "success",
        result: result
      })
    })
  }

  async delete(req, res) {
    pemasok.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: "success",
        data: null
      })
    })}
}

module.exports = new PemasokController;
