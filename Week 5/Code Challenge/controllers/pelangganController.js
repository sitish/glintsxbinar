const {pelanggan} = require('../models')



class PelangganController {

  async getAll(req,res) {
    pelanggan.find({}).then(result => {
        res.json({
          status: "success",
          data: result
        })
      });
  }

  async getOne(req,res) {
    pelanggan.findOne({
        _id: req.params.id
      }).then(result => {
        res.json({
          status: "success",
          data: result
        })
      })
  }

  async create(req,res) {
    pelanggan.create({
      nama:req.body.nama
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async update(req,res) {
    return pelanggan.updateOne({
      _id:req.params.id
    }, {
      $set: {
        nama :req.body.nama
      }
    }).then(() => {
      return pelanggan.findOne({
        _id:req.params.id
      })
    }).then(result => {
      res.json({
        status:'success',
        data: result
      })
    })
  }

  async delete(req,res) {
    return pelanggan.deleteOne({
      _id: req.params.id
    }).then(result =>{
      res.json({
        status:'success',
        data:null
      })
    })
  }

}

module.exports = new PelangganController
