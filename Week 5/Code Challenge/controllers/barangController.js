const {barang,pemasok}=require('../models')

class BarangController{
    async getAll(req,res){
        barang.find({}).then(result => {
            res.json({
              status: "success",
              data: result
            })
          });
    }
    async getOne(req, res) {
        barang.findOne({
          _id: req.params.id
        }).then(result => {
          res.json({
            status: "success",
            data: result
          })
        })
      }
      async create(req, res) {
        const data = await Promise.all([
            pemasok.findOne({
            _id: req.body.id_pemasok
          })
        ])
        barang.create({
          "nama": req.body.nama,
          "harga": eval(req.body.harga),
          "image":req.file === undefined ? "" : req.file.filename,
          "pemasok": data
        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
      }
    
      async update(req, res) {
        const data = 
          pemasok.findOne({
            _id: req.body.id_pemasok
          })
        barang.findOneAndUpdate({
          _id: req.params.id
        }, {
            "nama": req.body.nama,
            "harga": eval(req.body.harga),
            "id_pemasok": req.body.id_pemasok
        }).then(() => {
          return barang.findOne({
            _id: req.params.id
          })
        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
      }
      async delete(req, res) {
        barang.delete({
          _id: req.params.id
        }).then(() => {
          res.json({
            status: "success",
            data: null
          })
        })
      }
}
module.exports=new BarangController