const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const transaksiRoutes = require("./routes/transaksiRoutes");
const barangRoutes = require("./routes/barangRoutes");
const pemasokRoutes = require("./routes/pemasokRoutes");
const pelangganRoutes = require("./routes/pelangganRoutes");

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use("/transaksi", transaksiRoutes);
app.use("/barang", barangRoutes);
app.use("/pemasok", pemasokRoutes);
app.use("/pelanggan", pelangganRoutes);
app.listen(3000);
