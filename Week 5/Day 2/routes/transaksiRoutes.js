const express =require('express')
const router=express.Router()
const TransaksiController=require('../controllers/transaksiController.js')
const transaksiValidator=require('../middlewares/validators/transaksiValidator')


router.get('/',TransaksiController.getAll)
router.get('/:id',TransaksiController.getOne)
router.post('/create', transaksiValidator.create, TransaksiController.create) // If accessing localhost:3000/create, it will go to transaksiValidator and create function in controller
router.put('/update/:id', transaksiValidator.update, TransaksiController.update) // If accessing localhost:3000/update/:id, it will go to transaksiValidator and update function in controller
router.delete('/delete/:id', TransaksiController.delete) // If accessing localhost:3000/delete/:id, it will go to delete function in transaksiController


module.exports=router;