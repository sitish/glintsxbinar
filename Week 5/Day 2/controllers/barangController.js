const { ObjectID } = require("mongodb");
const client = require("../models/connection.js");

const penjualan = client.db("penjualan");
const barang = penjualan.collection("barang");
const pemasok = penjualan.collection("pemasok");
class BarangController {
  async getAll(req, res) {
    barang
      .find({})
      .toArray()
      .then((result) => {
        res.json({
          status: "success",
          data: result,
        });
      });
  }
  async getOne(req, res) {
    barang
      .findOne({
        _id: new ObjectID(req.params.id),
      })
      .then((result) => {
        res.json({
          status: "success",
          data: result,
        });
      });
  }
  async create(req, res) {
    const getPemasok = await pemasok.findOne({
      _id: new ObjectID(req.body.id_pemasok),
    });
    //const harga=eval(barang.barang.harga.toString())
    barang
      .insertOne(
        {
          nama: req.body.nama,
          harga: req.body.harga,
          pemasok: getPemasok,
        },
        {
          upsert: true,
        }
      )
      .then((result) => {
        res.json({
          status: "success",
          data: result.ops[0],
        });
      });
  }
  async update(req, res) {
    const getPemasok = await pemasok.findOne({
      _id: new ObjectID(req.body.id_pemasok),
    });
    barang
      .updateOne(
        {
          _id: new ObjectID(req.params.id),
        },
        {
          $set: {
            nama: req.body.nama,
            harga: req.body.harga,
            pemasok: getPemasok,
          },
        }
      )
      .then(() => {
        return barang.findOne({
          _id: new ObjectID(req.params.id),
        });
      })
      .then((result) => {
        res.json({
          status: "success",
          data: result,
        });
      });
  }
  async delete(req, res) {
    barang
      .deleteOne({
        _id: new ObjectID(req.params.id),
      })
      .then((result) => {
        res.json({
          status: "success",
          data: null,
        });
      });
  }
}
module.exports = new BarangController();
