const { ObjectID } = require('mongodb')
const client= require('../models/connection.js')

const penjualan = client.db('penjualan')
const transaksi= penjualan.collection('transaksi')
const pelanggan=penjualan.collection('pelanggan')
const barang=penjualan.collection('barang');


class TransaksiController{
    async getAll(req, res){
        transaksi.find({}).toArray().then(result=>{
            res.json({
                status:"succcess",
                data:result
            })
        })
    }
    async getOne(req,res){
        transaksi.findOne({
            _id : new ObjectID(req.params.id)
        }).then(result=>{
            res.json({
                status:"success",
                data:result
            })
        })
    }
    async create(req, res){
        const getBarang = await barang.findOne({
            _id:new ObjectID(req.body.id_barang)
        })
        const getPelanggan = await pelanggan.findOne({
            _id:new ObjectID(req.body.id_pelanggan)
        })
        let total = eval(getBarang.harga.toString()) * req.body.jumlah // Calculate total of transaksi

        transaksi.insertOne({
            barang:getBarang,
            pelanggan:getPelanggan,
            jumlah:req.body.jumlah,
            total:total

        }).then(result => {
            res.json({
                status: "success",
                data: result.ops[0]
            })

        })
    }
    async update(req, res){
        const getBarang = await barang.findOne({
            _id:new ObjectID(req.body.id_barang)
        })
        const getPelanggan = await pelanggan.findOne({
            _id:new ObjectID(req.body.id_pelanggan)
        })
        let total=eval(barang.barang.harga.toString())*req.body.jumlah
        
        transaksi.updateOne({
            _id: new ObjectId(req.params.id)},{
                $set: {
                    barang: getBarang,
                    pelanggan: getPelanggan,
                    jumlah: req.body.jumlah,
                    total: total
                }
        }).then(() => {
            return transaksi.findOne({
                _id: new ObjectId(req.params.id)
            })
        }).then(result => {
        res.json({
            status: 'success',
            data: result
            })
        })
    }
    async delete(req, res) {
        transaksi.deleteOne({
            _id: new ObjectID(req.params.id)
          }).then(result => {
            res.json({
              status: 'success',
              data: null
            })
          })
      }
}
module.exports=new TransaksiController