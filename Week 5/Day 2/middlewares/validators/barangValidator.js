const { check, validationResult } = require("express-validator");
const { ObjectID } = require("mongodb");
const client = require("../../models/connection.js");
const penjualan = client.db("penjualan");
const barang = penjualan.collection("barang");
const pemasok = penjualan.collection("pemasok");

module.exports = {
  create: [
    check("nama").custom((value) => {
      return barang.findOne({
        nama: value
      }).then((result)=>{
        if (result) {
          throw new Error("nama barang sudah ada");
        }
      })
    }),
    check("id_pemasok").custom((value) => {
      return pemasok
        .findOne({
          _id: new ObjectID(value),
        })
        .then((result) => {
          if (!result) {
            throw new Error("ID pemasok tidak ditemukan");
          }
        });
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped(),
        });
      }
      next();
    },
  ],
  update: [
    check("id").custom((value) => {
      return barang
        .findOne({
          _id: new ObjectID(value),
        })
        .then((result) => {
          if (!result) {
            throw new Error("ID Barang tidak ada");
          }
        });
    }),
    check("id_pemasok").custom((value) => {
      return pemasok
        .findOne({
          _id: new ObjectID(value),
        })
        .then((result) => {
          if (!result) {
            throw new Error("ID Pemasok tidak ada");
          }
        });
    }),
    check('nama').isLength({ min: 3 }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped(),
        });
      }
      next();
    },
  ],
  delete: [
    check("id").custom((value) => {
      return barang
        .findOne({
          _id: new ObjectID(value),
        })
        .then((result) => {
          if (!result) {
            throw new Error("ID Barang tidak ada");
          }
        });
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped(),
        });
      }
      next();
    },
  ],
};
