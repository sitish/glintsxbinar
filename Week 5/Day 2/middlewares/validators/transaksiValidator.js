const { check, validationResult } = require("express-validator");
const { ObjectID } = require("mongodb");
const client = require("../../models/connection.js");

module.exports = {
  create: [
    check("id_barang").custom((value) => {
      return client
        .db("penjualan")
        .collection("barang")
        .findOne({
          _id: new ObjectID(value),
        })
        .then((result) => {
          if (!result) {
            throw new Error("ID barang tidak ditemukan");
          }
        });
    }),
    check("id_pelanggan").custom((value) => {
      client
        .db("penjualan")
        .collection("pelanggan")
        .findOne({
          _id: new ObjectID(value),
        })
        .then((result) => {
          if (!result) {
            throw new Error("ID pelanggan tidak ditemukan");
          }
        });
    }),
  ],
  update: [
    check("id").custom((value) => {
      return client
        .db("penjualan")
        .collection("transaksi")
        .findOne({
          _id: new ObjectID(value),
        })
        .then((result) => {
          if (!result) {
            throw new Error("ID Transaksi tidak ada");
          }
        });
    }),
    check("id_barang").custom((value) => {
      return client
        .db("penjualan")
        .collection("barang")
        .findOne({
          _id: new ObjectID(value),
        })
        .then((result) => {
          if (!result) {
            throw new Error("ID Barang tidak ada");
          }
        });
    }),
    check("id_pelanggan").custom((value) => {
      return client
        .db("penjualan")
        .collection("pelanggan")
        .findOne({
          _id: new ObjectID(value),
        })
        .then((result) => {
          if (!result) {
            throw new Error("ID Pelanggan tidak ada");
          }
        });
    }),
    check("jumlah").isNumeric(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped(),
        });
      }
      next();
    },
  ]
};
