const {check, validationResult, matchedData, sanitize} = require('express-validator');
const { ObjectID } = require('mongodb');
const client = require('../../models/connection.js')

module.exports = {
  create: [
    check('nama').custom(value=> {
      return client.db('penjualan').collection('pemasok').findOne({
        nama: value
      }).then(result => {
        if(result){
          throw new Error('Nama pemasok sudah ada')
        }
      })
    }),
    check('name').isLength({ min: 3 }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  update: [
    check('id').custom(value => {
      return client.db('penjualan').collection('pemasok').findOne({
        _id: new ObjectID(value)
      }).then(result => {
        if (!result) {
          throw new Error('ID Transaksi tidak ada')
        }
      })
    }),
    check('name').isLength({ min: 3 }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  delete: [
    check('id').custom(value => {
      return client.db('penjualan').collection('pemasok').findOne({
        _id: new ObjectID(value)
      }).then(result => {
        if (!result) {
          throw new Error('ID Pemasok tidak ada')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ]
}