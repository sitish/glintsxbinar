class BangunDatar {

  constructor(name) {
    this.name = name
  }

  luas() {
    console.log('Luas bangun datar');
  }

  keliling() {
    console.log('Keliling bangun datar');
  }
}

// This class is child of BangunDatar
class PersegiPanjang extends BangunDatar {

  constructor(panjang, lebar) {
    super('Persegi Panjang')
    this.panjang = panjang
    this.lebar = lebar
  }

  // Overriding of BangunDatar
  luas() {
    return this.panjang * this.lebar
  }

  keliling() {
    return 2 * (this.panjang + this.lebar)
  }
}

class Persegi extends BangunDatar {

  constructor0(sisi) {
    super('Persegi')
    this.sisi = sisi
  }

  // Overriding
  luas() {
    return sisi ** 2
  }

  keliling() {
    return sisi * 4
  }
}

let persegipanjang = new PersegiPanjang(10, 10)
console.log(persegipanjang); // print property of persegipanjang
console.log(persegipanjang.luas())
console.log(persegipanjang.keliling());

let persegi = new Persegi(10)
