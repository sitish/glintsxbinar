const express = require('express') 
const router = express.Router();
const IndexController = require('../controllers/indexController.js')


router.get('/siti', IndexController.index);

module.exports = router; 
