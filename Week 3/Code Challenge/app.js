const express= require('express')
const app=express();
const indexRoute = require('./routes/indexRoute.js') 


app.get('/',(req,res)=>{
    res.send('<h1> This is just landing page. </h1><hr> please go to <a href="http://localhost:3000/siti">localhost:3000/siti</a> to access index')
})
app.get('/siti',indexRoute)

app.listen(3000)