// Import fs module
const fs = require('fs');

/* Start make promise object */
const readFile = options => file => new Promise((fulfill, reject) => {
  fs.readFile(file, options, (error, content) => {
    if (error) {
      return reject(error)
    }

    return fulfill(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})
/* Promise object end */

/* Make options variable for fs */
const read = readFile('utf-8')
/* End make options variable for fs */

/* Make an Array*/
result=[]
for(let i=1;i<=10;i++){
  result.push(read(`contents/content${i}.txt`))
}
/* end of make array*/


/* Async function */
async function mergedContent() {
  try {
    /* This recommended */
    // const result = await Promise.all([
    //   read('contents/content1.txt'),
    //   read('contents/content2.txt'),
    //   read('contents/content3.txt'),
    //   read('contents/content4.txt'),
    //   read('contents/content5.txt'),
    //   read('contents/content6.txt'),
    //   read('contents/content7.txt'),
    //   read('contents/content8.txt'),
    //   read('contents/content9.txt'),
    //   read('contents/content10.txt'),
    // ])
    // /* End this recommended */


    /*Use Array*/
    const merge = await Promise.all(result)
    /* End this Array*/

    await writeFile('contents/result.txt', merge.join(''))
  } catch (error) {
    throw error
  }

  // The best practice is:
  // return promise, not return value of promise
  // not also return use await
  return read('contents/result.txt')
}
/* Async function end */

// console.log(mergedContent()); // Promise (Pending)

// Start promise
mergedContent() // process read/write
  .then(result => {
    console.log('Success to read and write file, content:\n',result); // If success read/write file
  }).catch(error => {
    console.log('Failed to read/write file, content: ', error); // If error read/write file
  }).finally(() => {
    console.log('End!'); // run after success or error
  })
