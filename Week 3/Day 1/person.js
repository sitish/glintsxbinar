
var person=[{
    "name": "John",
    "status": "Positive"
 },
 {
    "name": "Mike",
    "status": "Suspect"
 },
 {
    "name": "Siska",
    "status": "Negative"
 },
 {
    "name": "Dimas",
    "status": "Positive"
 },
 {
    "name": "Aziz",
    "status": "Suspect"
 },
 {
    "name": "Dika",
    "status": "Negative"
 }
]
module.exports.data = person;