const Human=require('./human.js')

class Programmer extends Human {
    constructor(name,programmingLanguages) {
        super(name)
        this.programmingLanguages = programmingLanguages
    }
    introduce() {
        super.introduce()
        console.log(`I'm a programmer.`);
        this.code()
    }

    code() {
        console.log(
            "Code some",
            this.programmingLanguages[Math.floor(Math.random() * this.programmingLanguages.length)]
        );
    }
}
module.exports.programmer=Programmer