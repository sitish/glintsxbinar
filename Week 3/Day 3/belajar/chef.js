const Human=require('./human.js')

class Chef extends Human{
    constructor(name,cuisine){
        super(name)
        this.cuisine=cuisine;
    }

    introduce(){
        super.introduce()
        console.log(`I'm a chef. I can cook this several cuisines like ${this.cuisine} food`);
    }
}

module.exports.chef=Chef
