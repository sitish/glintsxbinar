// Import Bangun  class
const bangun = require('./bangun.js');

// This is class BangunDatar
class BangunRuang extends bangun {
  constructor(name) {
    super(name)

    // It is abstract class
    if (this.constructor === BangunRuang) {
      throw new Error('This is abstract')
    }
  }
    // instance method
    menghitungVolume() {
        console.log(`Menghitung Volume ${this.name}`);
      }
    
      // instance method
    menghitungLuasPermukaan() {
        console.log(`Menghitung Luas Permukaan ${this.name}`);
      }
}
module.exports=BangunRuang