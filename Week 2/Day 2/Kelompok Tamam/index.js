/**
 * TIDAK SEMUA ADA VALIDASI isNaN
 * operation==1 artinya mencari luas permukaan
 * operation==2 artinya mencari volume
 * utk CONE dan SPHERE ga ada fungsinya disini udah terlanjur buat di task kmren
 */
// Importing Module
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});


//import file path
const formula2D  = require("./formula2D.js");
const formula3D  = require("./formula3D.js");
const cone = require("./cone.js");
const sphere= require("./sphere.js");


var twoDShape = ["Circle", "Square", "Rectangle", "Parallelogram"];
var threeDShape = [
  "Cube",
  "Beam",
  "Cone",
  "Sphere"
  
];
var operation2D=['Area','Perimeter'];
var operation3D=['Surface Area', 'Volume'];

function selectDimention() {
  console.log("=================================");
  console.log(
    "Geometry Calculator \n1. Plane Shape (2D) \n2. Solid Shape (3D)"
  );
  rl.question("Enter the number that you want: ", (num) => {
    console.log("=================================");
    console.log("Shapes:");
    if (num == 1) {
      for (let i = 0; i < twoDShape.length; i++) {
        console.log(i+1 + ". "+twoDShape[i]);
      }
      selectShape(num)
    }else if(num==2){
      for (let i = 0; i < threeDShape.length; i++) {
        console.log(i+1 + ". "+threeDShape[i]);
      }
      selectShape(num)
    }else{
      rl.close()
    }
  });
}

function selectShape(num){
  rl.question("Enter the number of shape:", shape=>{
    if(num==1){
      console.log(`\n=========${twoDShape[shape-1]}=========`)
      console.log("Operation \n1. Area\n2. Perimeter");
      selectOperation(num,shape)
    }else if(num==2){
      console.log(`\n=========${threeDShape[shape-1]}=========`)
      console.log("Operation \n1. Surface Area\n2. Volume");
      selectOperation(num,shape)
    }else{
      rl.close
    }
    
  });

}

function selectOperation(num,shape){
rl.question("Enter the number of operation:", operation=>{
  if(num==1){
    console.log(`\n=========${operation2D[operation-1]} Of ${twoDShape[shape-1]}=========`);
    findOperation(twoDShape[shape-1],operation);
    
  }else if(num==2){
    console.log(`\n=========${operation3D[operation-1]} Of ${threeDShape[shape-1]}=========`);
    findOperation(threeDShape[shape-1],operation);
  }

})
}

function findOperation(shapeName,operation){
// twoDShape = ["Circle", "Square", "Rectangle", "parallelogram"];
if(shapeName==twoDShape[0]){
  formula2D.circle(operation);
}else if(shapeName==twoDShape[1]){
  formula2D.square(operation);
}else if(shapeName==twoDShape[2]){
  formula2D.rectangle(operation);
}else if(shapeName==twoDShape[3]){
  formula2D.parallelogram(operation);
}
//threeDShape = ["Cube",Beam","Cone","Sphere"];
else if(shapeName==threeDShape[0]){
  formula3D.cube(operation);
}else if(shapeName==threeDShape[1]){
  formula3D.beam(operation);
}else if(shapeName==threeDShape[2]){
  cone.cone(operation);
}else if(shapeName==threeDShape[3]){
  sphere.sphere(operation)
}
else {
  rl.close();
}

}
  
selectDimention();
module.exports.rl = rl
