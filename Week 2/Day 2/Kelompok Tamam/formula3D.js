/**
 * TIDAK SEMUA ADA VALIDASI isNaN
 * operation==1 artinya mencari luas permukaan
 * operation==2 artinya mencari volume
 * utk CONE dan SPHERE ga ada fungsinya disini udah terlanjur buat di task kmren
 */
const index = require('./index.js')

//FUNGSI KUBUS
function cube(operation){
    if(operation==1){
      index.rl.question("side length:",a=>{
        console.log(`Result: ${6*(a**2)}`);
        console.log("======================================");
        index.rl.close()
      })    
    }else if(operation==2){
      index.rl.question("side length:",a=>{
        console.log(`Result: ${a**3}`)
        console.log("======================================");
        index.rl.close()
      })
    }
  }

//FUNGSI BALOK
  function beam(operation){
    if(operation==1){
      index.rl.question("width:",w=>{
        index.rl.question("length:",l=>{
            index.rl.question("height:",h=>{
                console.log(`Result: ${2*((l*w) + (l*h) + (w*h))}`);
                console.log("======================================");
                index.rl.close()
            })

        })
      })
    }else if(operation==2){
        index.rl.question("width:",w=>{
            index.rl.question("length:",l=>{
                index.rl.question("height:",h=>{
                    console.log(`Result: ${w*l*w}`);
                    console.log("======================================");
        index.rl.close()
                })
    
            })
          })
    }
  }



  module.exports.cube = cube;
  module.exports.beam = beam;