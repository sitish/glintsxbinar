const { Barang, Pemasok } = require("../models");
const {check,validationResult,matchedData,  sanitize,} = require("express-validator"); //form validation & sanitize form params

class BarangController {
  constructor() {
    Pemasok.hasMany(Barang, {
      foreignKey: "id_pemasok",
    });
    Barang.belongsTo(Pemasok, {
      foreignKey: "id_pemasok",
    });
  }
  // Get All data from transaksi
  async getAll(req, res) {
    Barang.findAll({
      // find all data of Transaksi table
      attributes: ["id", "nama", "harga", "image",["createdAt", "waktu"]], // just these attributes that showed
      include: [
        {
          model: Pemasok,
          attributes: ["nama"], // just this attrubute from Barang that showed
        },
      ],
    }).then((barang) => {
      res.json(barang); // Send response JSON and get all of Transaksi table
    });
  }
  // Get One data from transaksi
  async getOne(req, res) {
    Barang.findOne({
      // find one data of Transaksi table
      where: {
        id: req.params.id, // where id of Transaksi table is equal to req.params.id
      },
      attributes: ["id", "nama", "harga", "image", ["createdAt", "waktu"]], // just these attributes that showed
      include: [
        {
          model: Pemasok,
          attributes: [["nama", "nama_pemasok"]], // just this attrubute from Barang that showed
        },
      ],
    }).then((barang) => {
      res.json(barang); // Send response JSON and get one of Transaksi table depend on req.params.id
    });
  }
  // Create Barang data
  async create(req, res) {
    Barang.create({
      id:req.body.id,
      nama: req.body.nama,
      harga: req.body.harga,
      image: req.file === undefined ? "" : req.file.filename,
      id_pemasok: req.body.id_pemasok,
    }).then((newBarang) => {
      // Send response JSON and get one of Transaksi table that we've created
      res.json({
        status: "success",
        message: "transaksi added",
        data: newBarang,
      });
    });
  }
  // Update Transaksi data
  async update(req, res) {
    const update = {
      nama: req.body.nama,
      harga: req.body.harga,
      image: req.file === undefined ? "" : req.file.filename,
      id_pemasok: req.body.id_pemasok
    };
   Barang.update(update, {
        where: {
          id: req.params.id,
        },
      }).then(affectedRow => {
        return Barang.findOne({ // find one data of Transaksi table
          where: {
            id: req.params.id  // where id of Transaksi table is equal to req.params.id
          }
        })
      })
      .then((b) => {
        res.json({
          status: "success",
          message: "Barang updated",
          data: b,
        });
      }).catch((error) => {
        console.error(error);
      }).finally(function() {
        // settled (fulfilled or rejected)
        console.log("error");
     });
  } // Soft delete Transaksi data

  async delete(req, res) {
    Barang.destroy({
      // Delete data from Transaksi table
      where: {
        id: req.params.id, // Where id of Transaksi table is equal to req.params.id
      },
    })
      .then((affectedRow) => {
        // If delete success, it will return this JSON
        if (affectedRow) {
          return {
            status: "success",
            message: "Barang deleted",
            data: null,
          };
        }

        // If failed, it will return this JSON
        return {
          status: "error",
          message: "Failed",
          data: null,
        };
      })
      .then((r) => {
        res.json(r); // Send response JSON depends on failed or success
      });
  }
}

module.exports = new BarangController();
