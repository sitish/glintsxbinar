const { Pemasok } = require("../models");
const {check,validationResult,matchedData,  sanitize,} = require("express-validator"); //form validation & sanitize form params

class PemasokController {
  constructor() {}
   
  // Get All data from Pemasok
  async getAll(req, res) {
    Pemasok.findAll({
      // find all data of Pemasok table
      attributes: ["id", "nama",["createdAt", "waktu"]], // just these attributes that showed
      
    }).then((result) => {
      res.json(result);
    });
  }
  // Get One data from transaksi
  async getOne(req, res) {
    Pemasok.findOne({
      where: {
        id: req.params.id, // where id of Pemasok table is equal to req.params.id
      },
      attributes: ["id", "nama",["createdAt", "waktu"]], // just these attributes that showed
      
    }).then((result) => {
      res.json(result); // Send response JSON and get one of Pemasok table depend on req.params.id
    });
  }

  async create(req, res) {
    Pemasok.create({
      nama: req.body.nama,
    }).then((newResult) => {
      // Send response JSON and return result
      res.json({
        status: "success",
        message: "data Pemasok added",
        data: newResult,
      });
    });
  }
  // Update Transaksi data
  async update(req, res) {
    const update = {
      nama: req.body.nama,
    };
   Pemasok.update(update, {
        where: {
          id: req.params.id,
        },
      }).then(affectedRow => {
        return Pemasok.findOne({ // find one data of Transaksi table
          where: {
            id: req.params.id  // where id of Transaksi table is equal to req.params.id
          }
        })
      })
      .then((b) => {
        res.json({
          status: "success",
          message: "Pelanggan updated",
          data: b,
        });
      }).catch((error) => {
        console.error(error);
      });
  } // Soft delete Transaksi data

  async delete(req, res) {
    Pemasok.destroy({
      // Delete data from Transaksi table
      where: {
        id: req.params.id, // Where id of Transaksi table is equal to req.params.id
      },
    })
      .then((affectedRow) => {
        // If delete success, it will return this JSON
        if (affectedRow) {
          return {
            status: "success",
            message: "Pelanggan deleted",
            data: null,
          };
        }

        // If failed, it will return this JSON
        return {
          status: "error",
          message: "Failed",
          data: null,
        };
      })
      .then((r) => {
        res.json(r); // Send response JSON depends on failed or success
      });
  }
}

module.exports = new PemasokController();
