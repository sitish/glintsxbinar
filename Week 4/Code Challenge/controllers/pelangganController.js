const { Pelanggan } = require("../models");
const {check,validationResult,matchedData,  sanitize,} = require("express-validator"); //form validation & sanitize form params

class PelangganController {

  // Get All data from transaksi
  async getAll(req, res) {
    Pelanggan.findAll({
      // find all data of Transaksi table
      attributes: ["id", "nama", ["createdAt", "waktu"]], // just these attributes that showed
      
    }).then((pelanggan) => {
      res.json(pelanggan); // Send response JSON and get all of Transaksi table
    });
  }

  async getOne(req, res) {
    Pelanggan.findOne({
      where: {
        id: req.params.id,
      },
      attributes: ["id", "nama",["createdAt", "waktu"]], // just these attributes that showed
      
    }).then((pelanggan) => {
      res.json(pelanggan); // Send response JSON and get one of Transaksi table depend on req.params.id
    });
  }
  // Create Barang data
  async create(req, res) {
    Pelanggan.create({
      nama: req.body.nama,
    }).then((newResult) => {
      // Send response JSON and get one of Transaksi table that we've created
      res.json({
        status: "success",
        message: "Pelanggan added",
        data: newResult,
      });
    });
  }
  // Update Transaksi data
  async update(req, res) {
    const update = {
      nama: req.body.nama,
    };
   Pelanggan.update(update, {
        where: {
          id: req.params.id,
        },
      }).then(affectedRow => {
        return Pelanggan.findOne({ // find one data of Transaksi table
          where: {
            id: req.params.id  // where id of Transaksi table is equal to req.params.id
          }
        })
      })
      .then((b) => {
        res.json({
          status: "success",
          message: "Pelanggan updated",
          data: b,
        });
      }).catch((error) => {
        console.error(error);
      });
  } // Soft delete Transaksi data

  async delete(req, res) {
    Pelanggan.destroy({
      // Delete data from Transaksi table
      where: {
        id: req.params.id, // Where id of Transaksi table is equal to req.params.id
      },
    })
      .then((affectedRow) => {
        // If delete success, it will return this JSON
        if (affectedRow) {
          return {
            status: "success",
            message: "Pelanggan deleted",
            data: null,
          };
        }

        // If failed, it will return this JSON
        return {
          status: "error",
          message: "Failed",
          data: null,
        };
      })
      .then((r) => {
        res.json(r); // Send response JSON depends on failed or success
      });
  }
}

module.exports = new PelangganController();
