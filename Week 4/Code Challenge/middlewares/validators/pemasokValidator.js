const {Pemasok } = require("../../models") // Import models
const { check, validationResult, matchedData, sanitize } = require('express-validator'); //form validation & sanitize form params

module.exports = {
    create: [
      check('nama').isLength({ min: 2 }),
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      },
    ],
    update: [
        //Set form validation rule
        check('id').isNumeric().custom(value => {
            return Pemasok.findOne({
                where: {
                    id: value
                }
            }).then(b => {
                if (!b) {
                    throw new Error('Id_pemasok not found');
                }
            })
        }),
        check('nama').isLength({ min: 2 }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        },
      ],
      delete: [
        //Set form validation rule
        check('id').isNumeric().custom(value => {
          return Pemasok.findOne({
            where: {
              id: value
            }
          }).then(b => {
            if (!b) {
              throw new Error('Pelanggan not found');
            }
          })
        }),
      ]
    };
      