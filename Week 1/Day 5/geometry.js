// Importing Module
const { hrtime } = require('process');
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

//FORMULA
function volumeOfCone(r, h){
    const result=(3.14)*(r**2)*h/3;
    return "Result = "+result;
}
function volumeOfSphere(r){
    const result=(4/3)*(3.14)*(r*r*r);
    return "Result = "+result;
}

// INPUT FOR CONE
function inputRadius() {
  rl.question("Enter value of radius! ", radius => {
      if (!isNaN(radius)) {
          inputHeight(radius)
      } else {
          console.log(`Radius must be a number`)
          inputRadius()
      }
  })
}
function inputHeight(radius){
  rl.question("Enter value of height! ", height => {
    if (!isNaN(height)) {
        console.log(volumeOfCone(radius, height))
        console.log("======================================");
        rl.close()
    } else {
        console.log(`Height must be a number`);
        inputHeight(radius)
    }
})
}
//INPUT FOR SPHERE
function inputRadiusForSphere() {
  rl.question("Enter value of radius! ", rad => {
      if (!isNaN(rad)) {
          console.log(volumeOfSphere(rad))
          console.log("======================================");
          rl.close()
      } else {
          console.log(`Radius must be a number`)
          inputRadiusForSphere()
      }
  })
}

function selectGeometry(num){
  if(num==1){
  console.log("\tVolume of Cone")
  inputRadius();
  }else if(num==2){
    console.log("\tVolume of Sphere")
    inputRadiusForSphere()
  }else{
    rl.close()
  }
}  

//OPENING
console.log("======================================");
console.log("Calculate Volume of\n1. Cone\n2. Sphere\n3. Exit")
rl.question("Enter the number of geometry [1-2]:", num => {
  console.log("======================================");
  selectGeometry(num)
})
