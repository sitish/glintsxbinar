
//import module
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function testSize(num) {
  // code below this line
  if(!isNaN(num)){
      if(num<5){
          return "Tiny"
      }else if(num<10){
          return "Small"
      }else if(num<15){
          return "Medium"
      }else if(num<20){
          return "Large"
      }else{
          return "Huge"
      }
  }else{
      rl.close()
  }

}
function testScore(num){
    if(!isNaN(num)){
        if(num>60){
            return "Passed"
        }else {
            return "Failed"
        }
    }else{
        rl.close()

    }
}
console.log("Menu: \n1. Test Score\n2. Test Size");
rl.question("Select number:", cek=>{
    if(cek==1){
        rl.question("\nInput number:",num=>{
            console.log(`Score result: ${testScore(num)}`)
            rl.close()
        })
    }else if(cek==2){
        rl.question("\nInput number:",num=>{
            console.log(`Size: ${testSize(num)}`)
            rl.close()

        })
    }else{
        rl.close()
    }
})
// console.log(testSize(0));
// console.log(testSize(5));
// console.log(testSize(10));
// console.log(testSize(17));
// console.log(testSize(25));
// console.log(testScore(50));
// console.log(testScore(90));

//testScore(50) should return "Failed"
//testScore(90) should return "Passed"

//testSize(0) should return "Tiny"
//testSize(5) should return "Small"
//testSize(10) should return "Medium"
//testSize(17) should return "Large"
//testSize(25) should return "Huge"