// Write if-else statements to fulfill the following conditions:
// if score morethan 60 you pass otherwise you failed

// Importing Module
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

function testScore(num) {
  // code below this line
if(num>60){
    return "Passed";
}else if(num<=60){
    return "Failed";
}else{
    rl.close()
}
}
rl.question("Insert number:", number=>{
    console.log(testScore(number));
    rl.close()
})
// console.log(testScore(50));
// console.log(testScore(90));

//testScore(50) should return "Failed"
//testScore(90) should return "Passed"