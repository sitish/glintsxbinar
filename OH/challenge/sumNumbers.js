//Declare and initialize a variable total to 0. Use a for loop to add the value of each element of the myArr array to total.
//total should equal 20.

var myArr = [ 2, 3, 4, 5, 10];
console.log("Data= "+myArr);
console.log("n(Data): "+myArr.length+"\n");
// Only change code below this line

//CARA 1: pake variabel untuk menampungnya yaitu sum
console.log("CARA 1:variabel penampung");
var total=0;

//Pake for
for(var i=0;i<myArr.length;i++){
    total+=myArr[i]
}

console.log("total: "+total);

//CARA 2:metode Rekursif
/** 
 * sum([2,3,4,5,6], 5)
 * data[n-1]+sum(data,n-1);
 * 
  -> 6 + sum([2,3,4,5,6], 4)
  -> 5 + 6 + sum([2,3,4,5,6], 3)
  -> 4 + 5 + 6 + sum([2,3,4,5,6], 2)
  -> 3 + 4 + 5 + 6 + sum([2,3,4,5,6], 1)
  -> 2 + 3 + 4 + 5 + 6 + sum([2,3,4,5,6], 0) =>udah nol
  -> 2 + 3 + 4 + 5 + 6 + 0)
  -> 20
 */
console.log("\nCARA 2: Recursive");

//Rekursif dari belakang
function sumDesc(data, n) {
   
  return n <=0  ?  0 : data[n-1]+sumDesc(data, n-1);

}
console.log("sum Asc:"+sumDesc(myArr,myArr.length));


//Rekursif dari depan
function sumAsc(data,n=0) {
  
  return n >=data.length  ?  0 : data[n++]+sumAsc(data,n++);

}
console.log("sum Desc:"+sumAsc(myArr));


