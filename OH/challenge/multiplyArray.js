
function multiplyAll(arr) {
    var product = 1;
    // Only change code below this line
    
//PAKE NESTED FOR LOOP
  for(let i=0;i<arr.length;i++){
      for(let j=0;j<arr[i].length;j++){
          product*=arr[i][j]
      }
  }
    // Only change code above this line
    return product;
  }

  //PAKE FLAT ARRAY DAN RECURSIVE
  function multiply(data,n=0) {
    var newData=data.flat()
      return n >=newData.length  ?  1 : newData[n++]*multiply(newData,n++);
  
  }
console.log(  multiplyAll([[1,2],[3,4],[5,6,7]]));

  //multiplyAll([[1,2],[3,4],[5,6,7]]) should return 5040

console.log(multiply([[1,2],[3,4],[5,6,7]]));