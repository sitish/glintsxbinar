//import module
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
function isEmptyOrSpaces(str) {
    return str === null || str.trim()===""  
}
  
function multiply(num1, num2) {
    var result=num1*num2;
    return result;
}
function tryAgain(){
    rl.question(`Do you want to try again?[y/n]`,cek=>{
        if(cek=='y'){
        inputNumber()
        }else{
        rl.close()
        }
    })
}
function inputNumber(){
console.log("\n======Multiply Numbers======");
rl.question("first number: ", num1=>{
    rl.question("second number: ",num2=>{
            if(isEmptyOrSpaces(num1)||isEmptyOrSpaces(num2)){
                console.log(`\nPlease provide a value!\n`); 
                tryAgain()
            }else{
                if(!isNaN(num1)&&!isNaN(num2)){
                    console.log(`\nResult: ${multiply(num1, num2)}\n`);
                    tryAgain()
                }else{
                    console.log("\nPlease provide a number only!\n");
                   tryAgain()
                }
            }
        })
    })
}
inputNumber()